package com.ciber.training.tdd.jobmanager.repository;

import static org.junit.Assert.assertEquals;

import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class JobRepositoryTest {
	
	@Autowired
	JobRepository jobRepository;
	
	
	@Autowired
	TestEntityManager testEntityManager;
	
	@Test
	public void save_NewJob_Success() throws Exception {
		String id = UUID.randomUUID().toString();
		JobData testJobData = new JobData(id, "Test Job");
		
		jobRepository.save(testJobData);
		
		JobData savedJobData = jobRepository.findOne(id);
		assertEquals(id, savedJobData.getJobId());
		assertEquals("Test Job", savedJobData.getJobDescription());
		
	}

}
