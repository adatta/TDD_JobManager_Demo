package com.ciber.training.tdd.jobmanager.controller;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.ciber.training.tdd.jobmanager.service.JobManagerService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@WebMvcTest(JobManagerController.class)
public class JobManagerControllerTest {
	
	@Autowired
	MockMvc mvc;
	
	@MockBean
	private JobManagerService jobManagerService;

	@Test
	public void addJob_postCallToReachEndpoint_success() throws Exception{
		Job job = new Job("1001", "Standpipe Inspection");
		
		BDDMockito.when(this.jobManagerService.createNewJob(job))
		  .thenReturn(job);
		
		mvc.perform(post("/jobmanager/v1/jobs")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(job)))
		.andExpect(status().isCreated()).andReturn();
	}
	
	@Test
	public void addJob_postCallWithURIFormed_CorrectURI() throws Exception{
		Job job = new Job("1001", "Standpipe Inspection");
		
		BDDMockito.when(this.jobManagerService.createNewJob(job))
		  .thenReturn(job);
		
		MvcResult result = mvc.perform(post("/jobmanager/v1/jobs")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(job)))
		.andExpect(status().isCreated())
		.andReturn();
		
		String location = result.getResponse().getHeaders(HttpHeaders.LOCATION).get(0);
		assertTrue(location.contains("/jobmanager/v1/jobs/1"));
	}
	
	@Test
	public void addJob_CreateNewJobWithoutIdPassedIn_Created() throws Exception {
		Job job = new Job("Standpipe Inspection");
		
		BDDMockito.when(this.jobManagerService.createNewJob(job))
				  .thenReturn(new Job("1002","Standpipe Inspection"));
		
		MvcResult result = mvc.perform(post("/jobmanager/v1/jobs")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(job)))
		.andExpect(status().isCreated())
		.andReturn();
		
		Job createdJob = new ObjectMapper().readValue(result.getResponse().getContentAsString(), Job.class);
		assertTrue("1002".equals(createdJob.getJobId()));
		
		
	}

}
