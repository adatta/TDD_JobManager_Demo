package com.ciber.training.tdd.jobmanager.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ciber.training.tdd.jobmanager.controller.Job;
import com.ciber.training.tdd.jobmanager.repository.JobRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JobManagerServiceTest {
	
	private JobManagerService jobManagerService;
	
	@Autowired
	private JobRepository jobRepository;
	
	@Before
	public void before() {
		jobManagerService = new JobManagerService(jobRepository);
	}
		

	@Test
	public void createNewJob_WithIdPassedIn_NewResourceWithPassedInId() {
		
		Job newJob = jobManagerService.createNewJob(new Job("1003","Standard Inspection"));
		
		assertEquals("1003", newJob.getJobId());
	}
	
	@Test
	public void createNewJob_WithoutIdPassedIn_NewResourceWithUniqueIdCreated() {
		
		Job newJob = jobManagerService.createNewJob(new Job("Standard Inspection"));
		Job newJob1 = jobManagerService.createNewJob(new Job("Standard Inspection"));
		
		assertNotEquals(newJob.getJobId(), newJob1.getJobId());
	}

}
