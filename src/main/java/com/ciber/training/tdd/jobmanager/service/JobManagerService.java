package com.ciber.training.tdd.jobmanager.service;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.ciber.training.tdd.jobmanager.controller.Job;
import com.ciber.training.tdd.jobmanager.repository.JobData;
import com.ciber.training.tdd.jobmanager.repository.JobRepository;

@Component
public class JobManagerService {

	@Autowired
	JobRepository jobRepository;
	
	public JobManagerService(JobRepository jobRepository) {
		this.jobRepository = jobRepository;
	}
	public Job createNewJob(Job job) {
		String id = job.getJobId();
		if(StringUtils.isEmpty(id)) {
			id=UUID.randomUUID().toString();
		}
		JobData jobData = jobRepository.save(new JobData(id,job.getJobDescription()));
		
		return  new Job(jobData.getJobId(),jobData.getJobDescription());
	}

}
