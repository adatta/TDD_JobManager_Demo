package com.ciber.training.tdd.jobmanager.controller;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import com.ciber.training.tdd.jobmanager.service.JobManagerService;

@RestController
@RequestMapping(value="/jobmanager/v1")
public class JobManagerController {
	@Autowired
	private JobManagerService jobManagerService;
	
	@PostMapping(value="/jobs",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Job> addJob(@RequestBody Job job) {
		
		Job newJob = jobManagerService.createNewJob(job);
		URI uri = MvcUriComponentsBuilder.fromController(getClass())
				.path("/jobs/{id}")
				.buildAndExpand(newJob.getJobId())
				.toUri();
		return ResponseEntity.created(uri).body(newJob);
	}

}
