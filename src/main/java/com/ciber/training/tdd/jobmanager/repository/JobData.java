package com.ciber.training.tdd.jobmanager.repository;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Job")
public class JobData {

	@Id
	@Column(name="id")
	private String jobId;
	
	@Column(name="job_description")
	private String jobDescription;
	
	public JobData() {
		super();
	}
	
	public JobData(String jobDescription) {
		super();
		this.jobDescription = jobDescription;
	}
	
	public JobData(String jobId, String jobDescription) {
		super();
		this.jobId = jobId;
		this.jobDescription = jobDescription;
	}
	
	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getJobDescription() {
		return jobDescription;
	}

	public void setJobDescription(String jobDescription) {
		this.jobDescription = jobDescription;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((jobId == null) ? 0 : jobId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JobData other = (JobData) obj;
		if (jobId == null) {
			if (other.jobId != null)
				return false;
		} else if (!jobId.equals(other.jobId))
			return false;
		return true;
	}

}
