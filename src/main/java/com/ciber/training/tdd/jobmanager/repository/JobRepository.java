package com.ciber.training.tdd.jobmanager.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public interface JobRepository extends CrudRepository<JobData, String>{

}
